import { Component, Input, OnInit } from '@angular/core';
import { LeadsService } from '../../_services/leads.service';
declare var $;

@Component({
  selector: 'app-area-contact',
  templateUrl: './area-contact.component.html',
  styleUrls: ['./area-contact.component.css']
})
export class AreaContactComponent implements OnInit {

  @Input() city: string = "";
  @Input() district: string = "";
  @Input() community: string = "";
  
  errMsg: string;

  member_address: string = '4656 Westwinds Dr NE #601, Calgary, AB T3J 3Z5';
  phone_list = [
    {phone_number: '403-807-1616', description: 'cell'}
  ];

  str_first_name: string = '';
  str_last_name: string = '';
  str_email: string = '';
  str_phone: string = '';
  str_msg: string = '';

  addMessage: boolean = false;

  typeList = [];
  subtypeList = [];

  str_type: string = null;
  str_subtype: string = null;
  isDisable: boolean = true;

  constructor(private leadsService: LeadsService) { }

  ngOnInit(): void {
    this.getLeadTypeList();
  }

  request_detail() {
    var address = "";
    
    if(this.community != "") address += this.community + ", ";
    if(this.district != '') address += this.district + ", ";
    address += this.city;

    this.leadsService.saveLeads('area', address, this.str_first_name, this.str_last_name, this.str_email, this.str_phone, this.str_msg, this.str_type, this.str_subtype).subscribe(
      data => {
        console.log(data);
      },
      err => {
        this.errMsg = err.message;
      }
    )
  }

  inputChange(newVal) {
    if(this.str_first_name != '' && this.str_last_name != '' && this.str_email != '' && this.str_phone != '') {
      this.isDisable = false;
    } else {
      this.isDisable = true;
    }
  }

  showMessage() {
    this.addMessage = true;
  }

  getLeadTypeList() {
    this.leadsService.getLeadTypeList().subscribe(
      data => {
        this.typeList = data;

        this.getLeadSubTypeList(this.typeList[0]['_id']);
      },
      err => {
        this.errMsg = err.message;
      }
    )
  }

  getLeadSubTypeList(typeId: string) {
    this.leadsService.getLeadSubTypeList(typeId).subscribe(
      data => {
        this.subtypeList = data;
      },
      err => {
        this.errMsg = err.message;
      }
    )
  }

  onChangeType(event) {
    this.getLeadSubTypeList(event.value);

    // this.leadsService.ChangeType(selId, event.value, this.userInfo_member.id).subscribe(
    //   data => {
    //     console.log(data);
    //   },
    //   err => {
    //     this.error = err.message;
    //   }
    // )
  }

  onChangeSubType(event) {
    // this.leadsService.ChangeSubType(selId, event.value, this.userInfo_member.id).subscribe(
    //   data => {
    //     console.log(data);
    //   },
    //   err => {
    //     this.error = err.message;
    //   }
    // )
  }

}
