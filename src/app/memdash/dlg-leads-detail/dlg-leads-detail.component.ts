import { Component, Inject, OnInit } from '@angular/core';
import { LeadsService } from '../../_services/leads.service';
import { UserService } from '../../_services/user.service';
import { TokenStorageService } from '../../_services/token-storage.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import * as moment from 'moment';

interface DialogData {
  selId: string;
}

@Component({
  selector: 'app-dlg-leads-detai',
  templateUrl: './dlg-leads-detail.component.html',
  styleUrls: ['./dlg-leads-detail.component.css']
})
export class DlgLeadsDetailComponent implements OnInit {

  displayedColumns: string[] = ['date', 'team_member', 'comment'];
  teamList = [];
  error: string;
  successMsg: string;
  userInfo_member: any;
  leadInfo = {
    _id: '',
    first_name: '',
    last_name: '',
    email: '',
    phone: '',
    byTeam: '',
    status: '',
    comment: '',
    comment_list: [
      {
        date: new Date(),
        team_member: '',
        comment: ''
      }
    ],
    contact_again: new Date(),
    type: '',
    subtype: '',
    accepted: '0',
    perm_phone: '0',
    perm_email: '0',
    perm_text: '0'
  };

  // statusList = [
  //   { value: 'new', viewValue: 'new'},
  //   { value: 'read', viewValue: 'read'},
  //   { value: 'called', viewValue: 'called'},
  //   { value: 'first time contacted', viewValue: 'first time contacted'},
  //   { value: 'no contact need to contact agian', viewValue: 'no contact need to contact agian'},
  //   { value: 'contacted', viewValue: 'contacted'},
  //   { value: 'lead is workable', viewValue: 'lead is workable'},
  //   { value: 'lead is not workable', viewValue: 'lead is not workable'},
  //   { value: 'progress', viewValue: 'progress'},
  //   { value: 'completed', viewValue: 'completed'},
  // ];

  statusList = [];

  commentList = [];
  typeList = [];
  subtypeList = [];

  str_comment: string;
  show_comment: boolean = false;

  public date: moment.Moment;
  public disabled = false;
  public showSpinners = true;
  public showSeconds = false;
  public touchUi = false;
  public enableMeridian = false;
  public minDate: moment.Moment;
  public maxDate: moment.Moment;
  public stepHour = 1;
  public stepMinute = 1;
  public stepSecond = 1;
  public disableMinute = false;
  public hideTime = false;
  public color = 'accent';

  public dateControl = new FormControl(new Date());

  str_date = '';
  
  constructor(private leadsService: LeadsService, private userService: UserService, private tokenStorageService: TokenStorageService, private ref: MatDialogRef<DlgLeadsDetailComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData) { 
    this.userInfo_member = this.tokenStorageService.getUser_member();
  }

  ngOnInit(): void {
    this.getTeamListAll();

    this.getLeadById(this.data.selId);

    this.getLeadTypeList();

    this.getLeadStatusList();
  }

  getLeadById(selId) {
    
    this.leadsService.getLeadById(selId).subscribe(
      data => {
        this.leadInfo = data[0];
        this.commentList = data[0].comment_list;

        var dTmp = new Date(this.leadInfo.contact_again);
        this.str_date = dTmp.getFullYear() + "-" + this.addZero(dTmp.getMonth() + 1) + "-" + this.addZero(dTmp.getDate()) + " " + this.addZero(dTmp.getHours()) + ":" + this.addZero(dTmp.getMinutes()) + ":" + this.addZero(dTmp.getSeconds());
        console.log(this.str_date);
        this.date = moment({ y: dTmp.getFullYear(), M: dTmp.getMonth(), d: dTmp.getDate(), h: dTmp.getHours(), m: dTmp.getMinutes(), s: dTmp.getSeconds(), ms: dTmp.getMilliseconds()}); 
      },
      err => {
        this.showErrorMsg(err);
      }
    )
  }

  getTeamListAll() {
    this.userService.getTeamListAll().subscribe(
      data => {
        this.teamList = data.items;
      },
      err => {
        this.showErrorMsg(err);
      }
    )
  }

  getLeadTypeList() {
    this.leadsService.getLeadTypeList().subscribe(
      data => {
        this.typeList = data;

        this.getLeadSubTypeList(this.leadInfo.type);
      },
      err => {
        this.showErrorMsg(err);
      }
    )
  }

  getLeadSubTypeList(typeId: string) {
    this.leadsService.getLeadSubTypeList(typeId).subscribe(
      data => {
        this.subtypeList = data;
      },
      err => {
        this.showErrorMsg(err);
      }
    )
  }

  getLeadStatusList() {
    this.leadsService.getLeadStatusList().subscribe(
      data => {
        this.statusList = data;
      },
      err => {
        this.showErrorMsg(err);
      }
    )
  }

  onChangeMember(event, selId) {
    this.leadsService.changeByTeam(selId, event.value).subscribe(
      data => {
        console.log(data);
      },
      err => {
        this.showErrorMsg(err);
      }
    )
  }

  onChangeStatus(event, selId) {
    this.leadsService.changeStatus(selId, event.value, this.userInfo_member.id).subscribe(
      data => {
        console.log(data);
      },
      err => {
        this.showErrorMsg(err);
      }
    )
  }

  onChangeType(event, selId) {
    this.getLeadSubTypeList(event.value);

    this.leadsService.ChangeType(selId, event.value, this.userInfo_member.id).subscribe(
      data => {
        console.log(data);
      },
      err => {
        this.showErrorMsg(err);
      }
    )
  }

  onChangeSubType(event, selId) {
    this.leadsService.ChangeSubType(selId, event.value, this.userInfo_member.id).subscribe(
      data => {
        console.log(data);
      },
      err => {
        this.showErrorMsg(err);
      }
    )
  }

  // onChangeComment(event, selId) {
  //   console.log(this.userInfo_member._id);
  //   this.leadsService.changeComment(selId, event, this.userInfo_member.id).subscribe(
  //     data => {
  //       console.log(data);
  //     },
  //     err => {
  //       this.showErrorMsg(err);
  //     }
  //   )
  // }

  showComment() {
    this.show_comment = true;
  }

  saveCommnet() {

    this.leadsService.saveCommnet(this.data.selId, this.str_comment, this.userInfo_member.id, this.userInfo_member.first_name + ' ' + this.userInfo_member.last_name).subscribe(
      data => {
        console.log(data);

        this.getLeadById(this.data.selId);

        this.str_comment = '';
      },
      err => {
        this.showErrorMsg(err);
      }
    )
  }

  getDate(strDate) {
    var tmp = strDate.split("T");
    var time = tmp[1].split(".");

    return tmp[0] + ' ' + time[0];
  }

  addZero(n) {
    if(n < 10) return "0" + n;
    else return n;
  }

  ngAfterViewChecked() {
    this.dateControl.valueChanges.subscribe((value: any) => {
      var selDateTimeVal = this.dateControl.value._d.getFullYear() + "-" + this.addZero(this.dateControl.value._d.getMonth() + 1) + "-" + this.addZero(this.dateControl.value._d.getDate()) + " " + this.addZero(this.dateControl.value._d.getHours()) + ":" + this.addZero(this.dateControl.value._d.getMinutes()) + ":" + this.addZero(this.dateControl.value._d.getSeconds());

      if(this.str_date != selDateTimeVal) {
        this.str_date = selDateTimeVal;

        console.log(this.str_date);
        this.leadsService.ChangeContactAgain(this.leadInfo._id, this.str_date, this.userInfo_member.id).subscribe(
          data => {
            console.log(data);
          },
          err => {
            this.showErrorMsg(err);
          }
        )
      }
    })
  }

  phoneChecked(checked: boolean, selId) {
    var perm = checked ? "1" : "0";
    this.leadsService.permPhone(selId, perm, this.userInfo_member.id).subscribe(
      data => {
        console.log(data);
      },
      err => {
        this.showErrorMsg(err);
      }
    )
  }

  emailChecked(checked: boolean, selId) {
    var perm = checked ? "1" : "0";
    this.leadsService.permEmail(selId, perm, this.userInfo_member.id).subscribe(
      data => {
        console.log(data);
      },
      err => {
        this.showErrorMsg(err);
      }
    )
  }

  textChecked(checked: boolean, selId) {
    var perm = checked ? "1" : "0";
    this.leadsService.permText(selId, perm, this.userInfo_member.id).subscribe(
      data => {
        console.log(data);
      },
      err => {
        this.showErrorMsg(err);
      }
    )
  }

  showSuccessMsg(strMsg) {
    this.successMsg = strMsg;

    setTimeout(() => {
      this.successMsg = null;
      this.ref.close("success");
    }, 3000);
  }

  showErrorMsg(strMsg) {
    this.error = strMsg;

    setTimeout(() => this.error = null, 5000);
  }
}
